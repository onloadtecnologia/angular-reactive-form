import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Cliente } from 'src/app/models/cliente';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  statusCliente:boolean=false;
  cliente:Cliente=new Cliente();
  formCliente!:FormGroup;
  fileReader:FormData=new FormData(); 
  
  ngOnInit(): void {
    this.formCliente = this.fb.group({
      nome: new FormControl(null,[Validators.required]),
      telefone: new FormControl(null,[Validators.required]),
      email: new FormControl(null,[Validators.required,Validators.email]),
      foto: new FormControl(null,[Validators.required])      
    });
  }
  constructor(private fb: FormBuilder) { }

  save(){
    this.fileReader.append("nome",this.formCliente.get("nome")?.value);
    this.fileReader.append("telefone",this.formCliente.get("telefone")?.value);
    this.fileReader.append("email",this.formCliente.get("email")?.value);
    alert(this.fileReader.get("nome"))   
    
  }

}
